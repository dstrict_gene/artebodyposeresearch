# ArteBodyPoseResearch

Research on Real-Time body tracking

## Things to consider
1. Level of detail
- Bounding Box
- Pose Detection, Pose tracking
  - Head Pose
  - Body Pose
  - Finger pose
- Body Segmentation
- Multiperson
- 2D or 3D
2. Sensor Type
- Lidar
- Image
3. Approach
- Bottom-up: estimate each body joint first and then group them to form a unique pose
- Top-down: run a person detector first and estimate body joints within the detected bounding boxes

## Pose Estimation

ADD PICTURE

Human pose estimation and tracking is a computer vision task that includes detecting, associating, and tracking semantic key points.

### Popular Pose Estimation Methods to consider

1. **OpenPose**
- Developed by CMU, its a most popular bottom-up approacshes for multi-person human pose estimation. Supports multiple source of images.
2. HRNet
- bottom-up multi-person pose estimation and localize keypoints more precisely, especially for small person. 
3. AlphaPose
- It is useful for detecting poses in the presence of inaccurate human bounding boxes.
4. **Detectron2:DensePose**
- Developed by Facebook, Dense human pose estimation aims at mapping all human pixels of an RGB image to the 3D surface of the human body.
5. TensorFlow Pose Estimation
- low power pose estimation
6. **Mediapipe**
- Developed by google, focuses on single user and provides various single user tracking: face, iris, hands, pose, hair, oject, motion and KNIFT
- Very good but have to run box tracking to do multi-person tracking

###
| Name | Real-Time | Multi-Person | ID tracking|
| ---  | ---     | ---| ---|
| Mediapipe | yes | no, yes with crop |? |
| DensePose | yes |yes, but slow |? |
| OpenPose | yes |yes |? |


## TRIAL
### 1. Yolov7
- pose detection: fps ~60
- segmentation : requires detectron2 and detectron2 doesnt support windows

#### Install and RUN

Install Conda
- run anaconda prompt as ADMIN
```
//check current envs
conda info --envs

//create virtual envs with python 3.8
conda create -n yolov7 python=3.8

//enter env
conda active yolov7

//show env info
conda info

//add linux command (e.g. ls)
conda install m2-base

//install git
conda install -c anaconda git

//check nvcc and nivia-smi
nvcc --version
nvidia-smi

//check CUDA_HOME path
python
import os
print(os.environ.get('CUDA_PATH'))

//pytorch + cudnn
pip install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html

//pycocotools
pip install cython
pip3 install "git+https://github.com/philferriere/cocoapi.git#egg=pycocotools&subdirectory=PythonAPI"

// git clone
git clone https://github.com/WongKinYiu/yolov7.git
pip install -r requirements.txt



// yolov7
// download weights from https://github.com/WongKinYiu/yolov7
// detect pose from video
git checkout origin/pose
python detect.py --weights yolov7-w6-pose.pt --source people.mov --name people_pose --kpt-label
```


